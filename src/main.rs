#![recursion_limit = "512"]

mod app;
mod components;
mod pages;
mod router;
mod utils;

use app::App;

pub fn main() {
    utils::set_panic_hook();

    yew::Renderer::<App>::new().render();
}
