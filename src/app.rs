use yew::prelude::function_component;
use yew::{html, Html};
use yew_router::{router::BrowserRouter, Switch};

use crate::pages::*;
use crate::router::Routes;

fn switch(routes: Routes) -> Html {
    match routes {
        Routes::Home => html! {<HomePage/>},
        Routes::NotFound => html! { "Page not found" },
        _ => todo!(),
    }
}

#[function_component]
pub fn App() -> Html {
    html! {
        <BrowserRouter>
            <Switch<Routes> render={switch} />
        </BrowserRouter>
    }
}
