use yew::prelude::function_component;
use yew::{html, Html};

use crate::components::{image::Image, link_icon::Icon, text::TextBlock};

#[function_component]
pub fn HomePage() -> Html {
    html! {
        <div id="app">
            <div class="flex-column page-body">
                <TextBlock title=true strong=true>{ "Calli" }</TextBlock>
                <div class="flex-row center-row">
                    <Image id="icon" src="icon.png" />
                    <div class="flex-column">
                        <Icon name="Github" href="https://github.com/CalliEve"/>
                        <Icon name="Gitlab" href="https://gitlab.com/CalliEve/"/>
                        <Icon name="Email" href="mailto:me@calli.dev"/>
                        <Icon name="Telegram" href="https://t.me/CalliEve"/>
                    </div>
                </div>
                <TextBlock strong=true>
                    {"Rust  •  TypeScript  •  Python  •  Golang  •  C++"}
                    <br/>
                    {"Software Developer  •  DevOps Engineer  •  Student Master Computer Science  •  Discord Bot Developer"}
                </TextBlock>
                <TextBlock strong=true>{ "she/her" }</TextBlock>
                <TextBlock strong=true>{ "Programming Addicted Wolf" }</TextBlock>
            </div>
            <footer>{ "made with "}<a href="https://github.com/yewstack/yew">{ "Yew" }</a></footer>
        </div>
    }
}
