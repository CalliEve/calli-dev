use yew::prelude::function_component;
use yew::{html, Html, Properties};

use super::image::Image;

#[derive(Properties, Clone, Default, PartialEq)]
pub struct IconProps {
    pub href: String,
    pub name: String,
}

#[function_component]
pub fn Icon(props: &IconProps) -> Html {
    let image_id = format!("{}-icon", props.name.to_lowercase());
    let image_src = format!("{}.svg", &image_id);

    html! {
        <a id="link-icon" href={props.href.clone()}>
            <Image id={image_id} src={image_src}/>
        </a>
    }
}
