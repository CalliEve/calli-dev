use yew::prelude::function_component;
use yew::{html, Children, Html, Properties};

#[derive(Properties, Clone, Default, PartialEq)]
pub struct TextBlockProps {
    #[prop_or_default]
    pub id: String,
    pub children: Children,

    #[prop_or_default]
    pub strong: bool,
    #[prop_or_default]
    pub title: bool,
}

#[function_component]
pub fn TextBlock(props: &TextBlockProps) -> Html {
    if props.title && props.strong {
        html! {
            <h1 id={props.id.clone()} class="text-block strong">{ props.children.clone() }</h1>
        }
    } else if props.title {
        html! {
            <h1 id={props.id.clone()} class="text-block">{ props.children.clone() }</h1>
        }
    } else if props.strong {
        html! {
            <strong id={props.id.clone()} class="text-block">{ props.children.clone() }</strong>
        }
    } else {
        html! {
            <p id={props.id.clone()} class="text-block">{ props.children.clone() }</p>
        }
    }
}
