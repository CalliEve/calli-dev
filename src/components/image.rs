use yew::prelude::function_component;
use yew::{html, Html, Properties};

#[derive(Properties, Clone, PartialEq, Default)]
pub struct ImageProps {
    #[prop_or_default]
    pub id: String,
    pub src: String,
}

#[function_component]
pub fn Image(props: &ImageProps) -> Html {
    let src = format!("/images/{}", &props.src);

    html! {
        <img id={props.id.clone()} src={src}/>
    }
}
