use yew_router::prelude::*;

#[derive(Routable, Debug, Clone, PartialEq)]
pub enum Routes {
    #[at("/bio")]
    Bio,
    #[at("/keys")]
    Keys,
    #[at("/")]
    Home,
    #[not_found]
    #[at("/404")]
    NotFound,
}
