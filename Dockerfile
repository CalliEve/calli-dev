FROM rust:1.69 as builder

RUN cargo install --locked trunk && \
    rustup target add wasm32-unknown-unknown

WORKDIR /www/calli-dev

COPY . .

RUN trunk build

RUN ls ./dist

FROM nginx as final

WORKDIR /calli

COPY ./nginx /etc/nginx/
COPY ./run.sh .
COPY --from=builder /www/calli-dev/dist/ /etc/nginx/web/calli/
COPY --from=builder /www/calli-dev/images/ /etc/nginx/web/calli/images/

RUN mkdir /etc/nginx/logs && mkdir /usr/share/nginx/logs && chmod 777 ./run.sh

RUN ls /etc/nginx/web/calli/

ENTRYPOINT ["./run.sh"]
